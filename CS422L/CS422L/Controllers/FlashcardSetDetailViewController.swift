//
//  FlashcardSetDetailViewController.swift
//  CS422L
//
//  Created by Student Account  on 1/28/22.
//

import UIKit

class FlashcardSetDetailViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {
    
    @IBOutlet var tableView: UITableView!
    var cards: [Flashcard] = [Flashcard]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        let cardClass = Flashcard()
        tableView.delegate = self
        tableView.dataSource = self
        cards = Flashcard.getHardCodedFlashcards()
        
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return cards.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier:"SetTableCell", for: indexPath) as! FlashcardCollectionCell
        
        cell.cardTextLabel.text = cards[indexPath.row].term
        return cell
    }
    
    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            cards.remove(at: indexPath.row)
            tableView.deleteRows(at: [indexPath], with: .fade)
        }
    }
    

    @IBAction func addFlashcard(_ sender: Any) {
        let card:Flashcard = Flashcard()
        card.term = "New Term"
        card.definition = "New Definition"
        
        cards.append(card)
        tableView.reloadData()
    }
}
